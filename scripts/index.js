document.querySelector("input").onclick = function () {

	let drawCircle = document.querySelector("input");

	let newParagraph = document.createElement("p");
	newParagraph.textContent = "Введіть діаметр кола в px"
	drawCircle.after(newParagraph);

	let newInput = document.createElement("input");
	newParagraph.after(newInput);

	let newInputSubmit = document.createElement("input");
	newInputSubmit.setAttribute("type", "submit");
	newInputSubmit.setAttribute("value", "Малювати");
	newInput.after(newInputSubmit);

	let controlDiv = document.createElement("div"); // щоб кружечки перенеслись нижче
	newInputSubmit.after(controlDiv);
	

	newInputSubmit.onclick = function () {

		for (let n = 0; n < 10; n++) {
			for (let i = 0; i < 10; i++) {

				let circle = document.createElement("div");
				circle.className = ("circles")
				circle.style.width = newInput.value + "px";
				circle.style.height = newInput.value + "px";
				circle.style.borderRadius = "50%";
				circle.style.display = "inline-block"
				circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, ${Math.floor(Math.random() * 100)}%, ${Math.floor(Math.random() * 100)}%)`
				circle.onclick = () => circle.remove();

				document.body.append(circle);
			}
			let br = document.createElement("br");
			document.body.append(br);


		}


	}

}